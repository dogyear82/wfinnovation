﻿using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using NUnit.Framework;
using wfinnovation.API.FeatureFlags;
using wfinnovation.API.Repositories;

namespace wfinnovation.Test.FeatureFlags
{
    [TestFixture]
    class FeatureFlagsTest
    {
        private Mock<HttpMessageHandler> _handler;
        private HttpClient _client;

        [SetUp]
        public void Setup()
        {
            _handler = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            _client = new HttpClient(_handler.Object);
        }

        private HttpResponseMessage ConstructResponse(object obj)
        {
            return new HttpResponseMessage
            {
                Content = new StringContent(JsonConvert.SerializeObject(obj))
            };
        }

        private void SetupHandlerResponse(object obj)
        {
            var response = ConstructResponse(obj);

            _handler
                .Protected()
                .Setup<Task<HttpResponseMessage>>("SendAsync", ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>())
                .ReturnsAsync(response);
        }

        [Test]
        public async Task FlagIsActive_FlagWasTurnedOn_ReturnsTrue()
        {
            var responseContent = new FeatureFlag
            {
                Enabled = true
            };

            SetupHandlerResponse(responseContent);

            var featureFlag = new FeatureFlag<string>(_client, "the-flag");
            var featureFlagIsActive = await featureFlag.IsActive();

            Assert.True(featureFlagIsActive);
        }

        [Test]
        public async Task GetVariant_HasAdminRole_ReturnsBlueButtonVariant()
        {
            var responseContent = new EvaluateFlagResponse<string>
            {
                Value = "value"
            };

            SetupHandlerResponse(responseContent);

            var featureFlag = new FeatureFlag<string>(_client, "flag key");
            var variant = await featureFlag.GetVariant("flag evaluation context");

            Assert.AreEqual(responseContent.Value, variant);
        }
    }
}
