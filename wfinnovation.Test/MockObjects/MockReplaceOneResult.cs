﻿using MongoDB.Bson;
using MongoDB.Driver;

namespace wfinnovation.Test.MockObjects
{
    class MockReplaceOneResult : ReplaceOneResult
    {
        public override bool IsAcknowledged { get; }
        public override bool IsModifiedCountAvailable { get; }
        public override long MatchedCount { get; }
        public override long ModifiedCount { get; }
        public override BsonValue UpsertedId { get; }

        public MockReplaceOneResult(bool isAcknowledged = false, bool isModifiedCountAvailable = false, long matchedCount = 0,
            long modifiedCount = 0)
        {
            IsAcknowledged = isAcknowledged;
            IsModifiedCountAvailable = isModifiedCountAvailable;
            MatchedCount = matchedCount;
            ModifiedCount = modifiedCount;
        }
    }
}
