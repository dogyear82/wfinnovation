﻿using System.Linq;
using NUnit.Framework;
using wfinnovation.API;
using wfinnovation.API.Validators;

namespace wfinnovation.Test.Validators
{
    [TestFixture]
    class CustomerValidatorTest
    {
        private CustomerValidator _validator;

        [SetUp]
        public void Setup()
        {
            _validator = new CustomerValidator();
        }

        [Test]
        public void ValidateCustomer_MissingFirstName_ReturnsMissingFirstNameError()
        {
            var customer = new Customer
            {
                LastName = "Muggs"
            };

            var result = _validator.Validate(customer);

            Assert.False(result.IsValid);
            Assert.IsNotEmpty(result.Errors);
            var error = result.Errors.FirstOrDefault();
            Assert.IsNotNull(error);
            Assert.AreEqual("Please specify a first name", error.ErrorMessage);
        }
    }
}
