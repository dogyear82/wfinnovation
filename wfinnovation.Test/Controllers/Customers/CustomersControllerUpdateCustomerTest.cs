﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using wfinnovation.API;
using wfinnovation.Test.MockObjects;

namespace wfinnovation.Test.Controllers.Customers
{
    [TestFixture]
    public class CustomersControllerUpdateCustomerTest : CustomersControllerTestBase
    {
        [Test]
        public void UpdateCustomer_HappyPath_ReturnsOkResult()
        {
            var expectedResult = new MockReplaceOneResult(true, modifiedCount: 1);
            _customerRepo
                .Setup(repo => repo.UpdateCustomer(It.IsAny<Customer>(), It.IsAny<string>()))
                .Returns(expectedResult);

            var customerToInsert = new Customer
            {
                FirstName = "Joe"
            };
            var okResult = _controller.UpdateCustomer(customerToInsert, "1234");

            Assert.IsInstanceOf<OkResult>(okResult);
        }

        [Test]
        public void UpdateCustomer_HasModelStateError_ReturnsBadRequestResult()
        {
            const string errorKey = "error";
            const string errorMessage = "message";
            _controller.ModelState.AddModelError(errorKey, errorMessage);

            var badResult = _controller.UpdateCustomer(new Customer(), "1234");

            Assert.IsInstanceOf<BadRequestObjectResult>(badResult);
        }

        [TestCase("")]
        [TestCase(" ")]
        [TestCase(null)]
        public void UpdateCustomer_NoCustomerIdProvided_ReturnsBadRequestResult(string customerId)
        {
            var badResult = _controller.UpdateCustomer(new Customer(), customerId);

            Assert.IsInstanceOf<BadRequestObjectResult>(badResult);
            Assert.AreEqual("Missing Customer ID", ((BadRequestObjectResult)badResult).Value);
        }
    }
}
