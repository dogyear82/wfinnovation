﻿using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using wfinnovation.API;

namespace wfinnovation.Test.Controllers.Customers
{
    [TestFixture]
    public class CustomersControllerInsertCustomerTest : CustomersControllerTestBase
    {
        [Test]
        public void PostCustomer_HappyPath_Returns200Status()
        {
            var customer = new Customer()
            {
                LastName = "Muggs"
            };

            var result = _controller.InsertCustomer(customer);

            Assert.IsNotNull(result);
        }

        [Test]
        public void PostCustomer_HasModelStateError_ReturnsModelStateWithError()
        {
            const string errorKey = "error";
            const string errorMessage = "message";
            _controller.ModelState.AddModelError(errorKey, errorMessage);

            var result = _controller.InsertCustomer(new Customer());
            var badRequestResult = result.Result;
            var error = (SerializableError)((ObjectResult)badRequestResult).Value;
            var message = (string[])error[errorKey];

            Assert.AreEqual(errorMessage, message[0]);
            Assert.IsInstanceOf<BadRequestObjectResult>(badRequestResult);
        }
    }
}
