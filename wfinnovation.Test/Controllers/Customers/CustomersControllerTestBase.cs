using System.Net.Http;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using wfinnovation.API.Controllers;
using wfinnovation.API.FeatureFlags;
using wfinnovation.API.Repositories;

namespace wfinnovation.Test.Controllers.Customers
{
    public class CustomersControllerTestBase
    {
        protected Mock<ICustomerRepository> _customerRepo;
        protected Mock<ILogger<CustomersController>> _logger;
        protected CustomersController _controller;
        protected Mock<HttpMessageHandler> _handler;

        [SetUp]
        public void Setup()
        {
            _customerRepo = new Mock<ICustomerRepository>();
            _logger = new Mock<ILogger<CustomersController>>();
            _handler = new Mock<HttpMessageHandler>();
            var featureFlags = new Flags(new HttpClient(_handler.Object));
            _controller = new CustomersController(_logger.Object, _customerRepo.Object, featureFlags);
        }
    }
}