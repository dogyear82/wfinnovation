﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using wfinnovation.API;

namespace wfinnovation.Test.Controllers.Customers
{
    [TestFixture]
    public class CustomersControllerGetCustomerTest : CustomersControllerTestBase
    {
        [Test]
        public void GetCustomer_HappyPath_ReturnsCustomer()
        {
            var customerId = "1234";
            _customerRepo.Setup(x => x.GetCustomerById(It.IsAny<string>())).Returns(new Customer { Id = customerId });

            var result = _controller.GetCustomerById("123");
            var okResult = result.Result;
            var customer = (Customer)((ObjectResult)okResult).Value;

            Assert.IsInstanceOf<OkObjectResult>(okResult);
            Assert.AreEqual(customerId, customer.Id);
        }

        [TestCase(null)]
        [TestCase("")]
        [TestCase(" ")]
        public void GetCustomer_MissingCustomerId_ReturnsNull(string customerId)
        {
            const string expectedErrorMessage = "Missing customer ID";

            var result = _controller.GetCustomerById(customerId);
            var errorMessage = ((ObjectResult)result.Result).Value;

            Assert.AreEqual(expectedErrorMessage, errorMessage);
            Assert.IsInstanceOf<BadRequestObjectResult>(result.Result);
        }
    }
}
