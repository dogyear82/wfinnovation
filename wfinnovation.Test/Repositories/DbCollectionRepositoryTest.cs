﻿using MongoDB.Driver;
using Moq;
using NUnit.Framework;
using wfinnovation.API;
using wfinnovation.API.Configs;
using wfinnovation.API.Helpers;
using wfinnovation.API.Repositories;

namespace wfinnovation.Test.Repositories
{
    [TestFixture]
    class DbCollectionRepositoryTest
    {
        private const string CustomerCollectionName = "Customer Collection Name";

        private BankDatabaseConfig _config;
        private IDbCollectionRepository _dbCollectionRepository;
        private Mock<IMongoCollectionHandler> _dbReader;
        private Mock<IMongoClient> _dbClient;
        private Mock<IMongoDatabase> _database;

        [SetUp]
        public void Setup()
        {
            _config = new BankDatabaseConfig
            {
                CustomerCollectionName = CustomerCollectionName
            };

            _database = new Mock<IMongoDatabase>();

            _dbClient = new Mock<IMongoClient>();
            _dbClient.Setup(x => x.GetDatabase(It.IsAny<string>(), null)).Returns(_database.Object);

            _dbReader = new Mock<IMongoCollectionHandler>();
            _dbReader.Setup(x => x.GetDbClient()).Returns(_dbClient.Object);

            _dbCollectionRepository = new DbCollectionRepository(_config, _dbReader.Object);
        }

        [Test]
        public void GetCustomerCollection_HappyPath_ReturnsCustomerCollection()
        {
            var collectionMock = new Mock<IMongoCollection<Customer>>();

            _database.Setup(x => x.GetCollection<Customer>(It.IsAny<string>(), null))
                .Returns(collectionMock.Object);

            var collection = _dbCollectionRepository.GetCustomerCollection();

            Assert.IsInstanceOf<IMongoCollection<Customer>>(collection);
        }
    }
}
