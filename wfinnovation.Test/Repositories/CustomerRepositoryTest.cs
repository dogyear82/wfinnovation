﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Bson;
using MongoDB.Driver;
using Moq;
using NUnit.Framework;
using wfinnovation.API;
using wfinnovation.API.Helpers;
using wfinnovation.API.Repositories;
using wfinnovation.Test.MockObjects;

namespace wfinnovation.Test.Repositories
{
    [TestFixture]
    public class CustomerRepositoryTest
    {
        private CustomerRepository _customerRepo;
        private Mock<IDbCollectionRepository> _dbCollectionRepoMock;
        private Mock<IMongoCollectionHandler> _collectionHandler;
        private Mock<IMongoCollection<Customer>> _customerCollection;

        [SetUp]
        public void Setup()
        {
            _customerCollection = new Mock<IMongoCollection<Customer>>();

            _dbCollectionRepoMock = new Mock<IDbCollectionRepository>();
            _dbCollectionRepoMock.Setup(x => x.GetCustomerCollection()).Returns(_customerCollection.Object);

            _collectionHandler = new Mock<IMongoCollectionHandler>(); 

            _customerRepo = new CustomerRepository(_dbCollectionRepoMock.Object, _collectionHandler.Object);
        }

        [Test]
        public void GetAllCustomers_Success_ReturnsACollectionOfCustomers()
        {
            var customerCollection = new List<Customer>
            {
                new Customer()
            };
            SetupCollectionHandlerGet(customerCollection);

            var returnedCustomers = _customerRepo.GetAllCustomers();

            Assert.Greater(returnedCustomers.Count, 0);
        }

        [Test]
        public void GetAllCustomers_NoCustomersInDatabase_ReturnsAnEmptyCollectionOfCustomers()
        {
            SetupCollectionHandlerGet(new List<Customer>());

            var returnedCustomers = _customerRepo.GetAllCustomers();

            Assert.False(returnedCustomers.Any());
        }

        [Test]
        public void GetCustomerById_CustomerFound_ReturnsCustomer()
        {
            var expectedCustomer = new Customer()
            {
                Id = "1234"
            };
            var customerCollection = new List<Customer> {
                expectedCustomer
            };

            SetupCollectionHandlerGet(customerCollection);

            var customer = _customerRepo.GetCustomerById("1234");

            Assert.AreEqual(expectedCustomer.Id, customer.Id);
        }

        [Test]
        public void GetCustomerById_CustomerNotFound_ReturnsNull()
        {
            var customerCollection = new List<Customer>();

            SetupCollectionHandlerGet(customerCollection);

            var customer = _customerRepo.GetCustomerById("1234");

            Assert.IsNull(customer);
        }

        private void SetupCollectionHandlerGet(List<Customer> returnResult)
        {
            _collectionHandler.Setup(x => x.Find(It.IsAny<IMongoCollection<Customer>>(), It.IsAny<Expression<Func<Customer, bool>>>()))
                .Returns(returnResult);
        }

        [Test]
        public void InsertCustomer_InsertSuccessful_ReturnsInsertedCustomer()
        {
            var id = Guid.NewGuid().ToString();
            _collectionHandler
                .Setup(x => x.InsertOne(It.IsAny<IMongoCollection<Customer>>(), It.IsAny<Customer>()))
                .Callback<IMongoCollection<Customer>, Customer>((customerCollection, customer) => customer.Id = id)
                .Verifiable();

            var expectedCustomer = new Customer
            {
                FirstName = "FirstName",
                LastName = "LastName",
                Email = "Email",
                Phone = "Phone"
            };

            var insertedCustomer = _customerRepo.InsertCustomer(expectedCustomer);

            Assert.IsNotNull(insertedCustomer);
            Assert.IsNotNull(insertedCustomer.Id);
            Assert.AreEqual(expectedCustomer.FirstName, insertedCustomer.FirstName);
            Assert.AreEqual(expectedCustomer.LastName, insertedCustomer.LastName);
            Assert.AreEqual(expectedCustomer.Email, insertedCustomer.Email);
            Assert.AreEqual(expectedCustomer.Phone, insertedCustomer.Phone);
            Assert.AreEqual(expectedCustomer.Address, insertedCustomer.Address);
            Assert.AreEqual(expectedCustomer.Loans, insertedCustomer.Loans);
        }

        [Test]
        public void UpdateCustomer_UpdateSuccessful_ReturnsSuccessfulUpdateResult()
        {
            var isAcknowledged = true;
            var modifiedCount = 1;
            var expectedUpdateResult = new MockReplaceOneResult(isAcknowledged, modifiedCount: modifiedCount);
            _collectionHandler
                .Setup(x => x.ReplaceOne(It.IsAny<IMongoCollection<Customer>>(), It.IsAny<Expression<Func<Customer, bool>>>(), It.IsAny<Customer>(), null))
                .Returns(expectedUpdateResult);

            var updateResult = _customerRepo.UpdateCustomer(new Customer(), "123");

            Assert.True(expectedUpdateResult.IsAcknowledged == updateResult.IsAcknowledged);
            Assert.True(expectedUpdateResult.ModifiedCount == updateResult.ModifiedCount);
        }

        [TestCase(false, 0)]
        [TestCase(true, 0)]
        [TestCase(false, 1)]
        public void Updatecustomer_UpdateFailed_ReturnsFailedUpdateResult(bool isAcknowledged, long modifiedCount)
        {
            var expectedUpdateResult = new MockReplaceOneResult(isAcknowledged, modifiedCount: modifiedCount);
            _collectionHandler
                .Setup(x => x.ReplaceOne(It.IsAny<IMongoCollection<Customer>>(), It.IsAny<Expression<Func<Customer, bool>>>(), It.IsAny<Customer>(), null))
                .Returns(expectedUpdateResult);

            var updateResult = _customerRepo.UpdateCustomer(new Customer(), "123");

            Assert.True(expectedUpdateResult.IsAcknowledged == updateResult.IsAcknowledged);
            Assert.True(expectedUpdateResult.ModifiedCount == updateResult.ModifiedCount);
        }
    }
}
