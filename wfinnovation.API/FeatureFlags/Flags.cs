﻿using System.Net.Http;

namespace wfinnovation.API.FeatureFlags
{
    public class Flags
    {
        public FeatureFlag<RoleContext> Role { get; }
        public FeatureFlag<string> Feature { get; }

        public Flags(HttpClient client)
        {
            Role = new FeatureFlag<RoleContext>(client, "new-login");
            Feature = new FeatureFlag<string>(client, "feature");
        }
    }
}
