﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace wfinnovation.API.FeatureFlags
{
    public class FeatureFlag
    {
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public List<Variant> Variants { get; set; }
    }

    public class Variant
    {
        public Guid Id { get; set; }
        public string FlagKey { get; set; }
        public string Key { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }

    public class EvaluateFlagResponse<T>
    {
        public Guid RequestId { get; set; }
        public Guid EntityId { get; set; }
        public object RequestContext { get; set; }
        public bool Match { get; set; }
        public string FlagKey { get; set; }
        public string SegmentKey { get; set; }
        public DateTime Timestamp { get; set; }
        public string Value { get; set; }
        public decimal RequestDurationMillis { get; set; }
    }

    public class EvaluateFlagRequest<T>
    {
        [JsonProperty("flagKey")]
        public string FlagKey { get; set; }

        [JsonProperty("entityId")]
        public Guid EntityId { get; set; }

        [JsonProperty("context")]
        public T Context { get; set; }
    }

    public class RoleContext
    {
        [JsonProperty("role")]
        public string Role { get; set; }
    }

    public class FeatureFlag<T>
    {
        private readonly HttpClient _client;
        private readonly string _flagKey;
        private readonly string _url = "http://localhost:8080/api/v1";
        private readonly string _flagsUrl;
        private readonly string _evaluateUrl;

        public FeatureFlag(HttpClient client, string flagKey)
        {
            _client = client;
            _flagKey = flagKey;
            _flagsUrl = $"{_url}/flags";
            _evaluateUrl = $"{_url}/evaluate";
        }

        public async Task<bool> IsActive()
        {
            var flag = await GetFlag();
            return flag.Enabled;
        }

        private async Task<FeatureFlag> GetFlag()
        {
            var url = $"{_flagsUrl}/{_flagKey}";
            var response = await _client.GetAsync(url);
            return JsonConvert.DeserializeObject<FeatureFlag>(await response.Content.ReadAsStringAsync());
        }

        public async Task<string> GetVariant(T context)
        {
            var evaluation = await GetFlagEvaluation(context);
            return evaluation.Value;
        }

        private async Task<EvaluateFlagResponse<T>> GetFlagEvaluation(T context)
        {
            var request = new EvaluateFlagRequest<T>
            {
                FlagKey = _flagKey,
                EntityId = Guid.NewGuid(),
                Context = context
            };

            var postContent = new StringContent(JsonConvert.SerializeObject(request), Encoding.UTF8, "application/json");
            var response = await _client.PostAsync(_evaluateUrl, postContent);

            return JsonConvert.DeserializeObject<EvaluateFlagResponse<T>>(await response.Content.ReadAsStringAsync());
        }
    }
}
