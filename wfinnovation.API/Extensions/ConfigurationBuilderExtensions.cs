﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Microsoft.Extensions.Configuration;
using wfinnovation.API.Enums;

namespace wfinnovation.API.Extensions
{
    public static class ConfigurationBuilderExtensions
    {
        public static void BuildAppSettingsConfiguration(this IConfigurationBuilder config)
        {
            var environment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
            var configRoot = config.BuildBaseAppSettingsConfiguration(environment);

            var isProduction = environment == Env.Production.ToString();
            if (isProduction)
            {
                config.SubstituteAppSettingsWithKeyStoreValues(configRoot["KeyVaultConfig:VaultUrl"], configRoot["KeyVaultConfig:ClientId"], configRoot["KeyVaultConfig:CertificateThumbprint"], configRoot["KeyVaultConfig:SecretsPrefix"]);
            }
        }

        private static IConfigurationRoot BuildBaseAppSettingsConfiguration(this IConfigurationBuilder config, string environment)
        {
            return config.SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{environment}.json", true, true)
                .AddEnvironmentVariables()
                .Build();
        }

        private static void SubstituteAppSettingsWithKeyStoreValues(this IConfigurationBuilder config, string vaultUrl, string clientId, string certThumbprint, string secretsPrefix)
        {
            config.AddAzureKeyVault(vaultUrl,
                clientId,
                GetCert(certThumbprint),
                new PrefixKeyVaultSecretManager(secretsPrefix));
        }

        private static X509Certificate2 GetCert(string thumbprint)
        {
            var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);

            try
            {
                store.Open(OpenFlags.ReadOnly);

                var certCollection = store.Certificates.Find(X509FindType.FindByThumbprint, thumbprint, false);
                if (certCollection.Count == 0)
                {
                    throw new Exception("Certficiate not installed in the store");
                }

                return certCollection.OfType<X509Certificate2>().FirstOrDefault();
            }
            finally
            {
                store.Close();
            }
        }
    }
}
