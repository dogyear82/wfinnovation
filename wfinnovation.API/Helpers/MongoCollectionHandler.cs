﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Security.Authentication;
using System.Threading;
using MongoDB.Driver;
using wfinnovation.API.Configs;

namespace wfinnovation.API.Helpers
{
    public interface IMongoCollectionHandler
    {
        IMongoClient GetDbClient();

        IList<T> Find<T>(IMongoCollection<T> collection, Expression<Func<T, bool>> expression);

        void InsertOne<T>(IMongoCollection<T> collection, T document);

        ReplaceOneResult ReplaceOne<T>(IMongoCollection<T> collection, Expression<Func<T, bool>> filter, T replacement,
            ReplaceOptions options = null);

        DeleteResult DeleteOne<T>(IMongoCollection<T> collection, Expression<Func<T, bool>> filter,
            CancellationToken cancellationToken = default);
    }

    public class MongoCollectionHandler : IMongoCollectionHandler
    {
        private readonly BankDatabaseConfig _dbConfig;

        public MongoCollectionHandler(BankDatabaseConfig dbConfig)
        {
            _dbConfig = dbConfig;
        }

        public IMongoClient GetDbClient()
        {
            var url = new MongoUrl(_dbConfig.ConnectionString);
            var settings = MongoClientSettings.FromUrl(url);
            settings.SslSettings = new SslSettings { EnabledSslProtocols = SslProtocols.Tls12 };
            return new MongoClient(settings);
        }

        public IList<T> Find<T>(IMongoCollection<T> collection, Expression<Func<T, bool>> expression)
        {
            return collection.Find(expression).ToList();
        }

        public void InsertOne<T>(IMongoCollection<T> collection, T document)
        {
            collection.InsertOne(document);
        }

        public ReplaceOneResult ReplaceOne<T>(IMongoCollection<T> collection, Expression<Func<T, bool>> filter, T replacement, ReplaceOptions options = null)
        {
            return collection.ReplaceOne(filter, replacement, options);
        }

        public DeleteResult DeleteOne<T>(IMongoCollection<T> collection, Expression<Func<T, bool>> filter, CancellationToken cancellationToken = default)
        {
            return collection.DeleteOne(filter, cancellationToken);
        }
    }
}
