﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using wfinnovation.API.FeatureFlags;
using wfinnovation.API.Repositories;
using static System.String;

namespace wfinnovation.API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ILogger<CustomersController> _logger;
        private readonly ICustomerRepository _customerRepo;
        private readonly Flags _flags;

        public CustomersController(ILogger<CustomersController> logger, ICustomerRepository customerRepo, Flags flags)
        {
            _logger = logger;
            _customerRepo = customerRepo;
            _flags = flags;
        }

        /// <summary>
        /// Retrieves all customers from the database
        /// </summary>
        /// <returns>A list of Customers</returns>
        /// <response code="200">Customer is successfully found and returned to the user</response>
        /// <response code="404">There are no customers (;_;)</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<IList<Customer>> GetAllCustomers()
        {
            var customers = _customerRepo.GetAllCustomers();
            if (!customers.Any())
            {
                return NotFound("We do not yet have any customers");
            }

            return Ok(customers);
        }

        /// <summary>
        /// Retrieves customer data by the customer ID.
        /// </summary>
        /// <remarks></remarks>
        /// <param name="customerId"></param>
        /// <returns>Customer</returns>
        /// <response code="200">Customer is successfully found and returned to the user</response>
        /// <response code="400">No Customer ID was provided for lookup</response>
        /// <response code="404">No customer was found with the given Customer ID</response>
        [HttpGet("{customerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public ActionResult<Customer> GetCustomerById(string customerId)
        {
            if (IsNullOrWhiteSpace(customerId))
            {
                return BadRequest("Missing customer ID");
            }
            var customer = _customerRepo.GetCustomerById(customerId);
            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        /// <summary>
        /// Adds a customer to the database
        /// </summary>
        /// <remarks></remarks>
        /// <param name="customer"></param>
        /// <returns>Customer</returns>
        /// <response code="200">Customer was successfully added to the database</response>
        /// <response code="400">The submitted customer information is invalid</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Customer> InsertCustomer(Customer customer)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var customerResult = _customerRepo.InsertCustomer(customer);
            return Ok(customerResult);
        }

        /// <summary>
        /// Updates a customer
        /// </summary>
        /// <remarks></remarks>
        /// <param name="customer"></param>
        /// <param name="customerId"></param>
        /// <returns>Customer</returns>
        /// <response code="200">Customer was successfully updated in the database</response>
        /// <response code="400">The customer data and/or customer ID was invalid and/or not provided</response>
        /// <response code="422">Unable to delete the customer for some reason</response>
        [HttpPut("{customerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult UpdateCustomer(Customer customer, string customerId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (IsNullOrWhiteSpace(customerId))
            {
                return BadRequest("Missing Customer ID");
            }

            var updateResult = _customerRepo.UpdateCustomer(customer, customerId);

            if (!updateResult.IsAcknowledged || updateResult.ModifiedCount.Equals(0))
            {
                return Problem($"There was a problem and we were unable to update the customer with an ID of {customerId}", statusCode: 422);
            }
            return Ok();
        }

        /// <summary>
        /// Deletes a customer
        /// </summary>
        /// <remarks></remarks>
        /// <param name="customerId"></param>
        /// <returns>Customer</returns>
        /// <response code="200">Customer was successfully deleted form the database</response>
        /// <response code="400">A valid customer ID was not provided</response>
        /// <response code="422">Unable to delete the customer for some reason</response>
        [HttpDelete("{customerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult DeleteCustomer(string customerId)
        {
            if (IsNullOrWhiteSpace(customerId))
            {
                return BadRequest("Missing Customer ID");
            }
            var deleteResult = _customerRepo.DeleteCustomer(customerId);

            if (!deleteResult.IsAcknowledged || deleteResult.DeletedCount.Equals(0))
            {
                return Problem($"There was a problem and we were unable to delete the customer with an ID of {customerId}", statusCode: 422);
            }
            return Ok();
        }
    }
}
