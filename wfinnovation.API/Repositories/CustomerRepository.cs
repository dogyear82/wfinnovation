﻿using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;
using wfinnovation.API.Helpers;

namespace wfinnovation.API.Repositories
{
    public interface ICustomerRepository
    {
        IList<Customer> GetAllCustomers();
        Customer GetCustomerById(string customerId);
        Customer InsertCustomer(Customer customerIn);
        ReplaceOneResult UpdateCustomer(Customer customerIn, string customerId);
        DeleteResult DeleteCustomer(string customerId);
    }

    public class CustomerRepository : ICustomerRepository
    {
        private readonly IMongoCollection<Customer> _customers;
        private readonly IMongoCollectionHandler _collectionHandler;

        public CustomerRepository(IDbCollectionRepository collectionRepo, IMongoCollectionHandler collectionHandler)
        {
            _customers = collectionRepo.GetCustomerCollection();
            _collectionHandler = collectionHandler;
        }

        public IList<Customer> GetAllCustomers()
        {
            return _collectionHandler.Find(_customers, _ => true);
        }

        public Customer GetCustomerById(string customerId)
        {
            var customer = _collectionHandler.Find(_customers, customerSearch => customerSearch.Id == customerId);
            return customer.FirstOrDefault();
        }

        public Customer InsertCustomer(Customer customerIn)
        {
            _collectionHandler.InsertOne(_customers, customerIn);
            return customerIn;
        }

        public ReplaceOneResult UpdateCustomer(Customer customerIn, string customerId)
        {
            return _collectionHandler.ReplaceOne(_customers, customer => customer.Id == customerId, customerIn);
        }

        public DeleteResult DeleteCustomer(string customerId)
        {
            return _collectionHandler.DeleteOne(_customers, customer => customer.Id == customerId);
        }
    }
}
