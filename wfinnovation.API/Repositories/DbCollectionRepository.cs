﻿using MongoDB.Driver;
using wfinnovation.API.Configs;
using wfinnovation.API.Helpers;

namespace wfinnovation.API.Repositories
{
    public interface IDbCollectionRepository
    {
        IMongoCollection<Customer> GetCustomerCollection();
    }

    public class DbCollectionRepository : IDbCollectionRepository
    {
        private readonly BankDatabaseConfig _dbConfig;
        private readonly IMongoDatabase _database;

        public DbCollectionRepository(BankDatabaseConfig dbConfig, IMongoCollectionHandler reader)
        {
            _dbConfig = dbConfig;
            var dbClient = reader.GetDbClient();
            _database = dbClient.GetDatabase(dbConfig.DatabaseName);
        }

        public IMongoCollection<Customer> GetCustomerCollection()
        {
            return _database.GetCollection<Customer>(_dbConfig.CustomerCollectionName);
        }
    }
}
