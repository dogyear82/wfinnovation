﻿namespace wfinnovation.API.Configs
{
    public class BankDatabaseConfig
    {
        public string ConnectionString { get; set; }
        public string CustomerCollectionName { get; set; }
        public string DatabaseName { get; set; }
    }
}
