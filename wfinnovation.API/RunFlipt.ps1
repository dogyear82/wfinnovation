﻿function RunFlipt() {

    BEGIN {
        $fliptPath = "C:$env:HOMEPATH\flipt"

        if (-not(Test-Path -Path $fliptPath)) {
            mkdir $fliptPath
        }
    }

    PROCESS {
        docker run -d -p 8080:8080 -p 9000:9000 -v "$($fliptPath -replace '\\','/'):/var/opt/flipt" markphelps/flipt:latest
    }        

    END {}
}
