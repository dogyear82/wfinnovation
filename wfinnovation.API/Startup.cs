using System;
using System.IO;
using System.Net.Http;
using System.Reflection;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using wfinnovation.API.Configs;
using wfinnovation.API.FeatureFlags;
using wfinnovation.API.Helpers;
using wfinnovation.API.Repositories;
using wfinnovation.API.Validators;

namespace wfinnovation.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();

            services.AddMvc().AddFluentValidation(fv =>
            {
                fv.RegisterValidatorsFromAssemblyContaining<CustomerValidator>();
            });

            services.AddSingleton<IDbCollectionRepository, DbCollectionRepository>();
            services.AddSingleton<IMongoCollectionHandler, MongoCollectionHandler>();
            services.AddSingleton<ICustomerRepository, CustomerRepository>();
            services.AddSingleton(x => new Flags(new HttpClient()));

            // requires using Microsoft.Extensions.Options
            services.Configure<BankDatabaseConfig>( Configuration.GetSection(nameof(BankDatabaseConfig)));
            services.AddSingleton(sp => sp.GetRequiredService<IOptions<BankDatabaseConfig>>().Value);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Customers API",
                    Description = "API for creating and retrieving customer data",
                    Contact = new OpenApiContact
                    {
                        Name = "Tan Nguyen",
                        Email = "tan.nguyen2@wellsfargo.com",
                        Url = new Uri("https://www.linkedin.com/in/tan-nguyen-7196a949/"),
                    }
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true)
                .AllowCredentials());

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Customer API V1");
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
